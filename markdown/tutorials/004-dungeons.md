# Dungeons
---

Dungeons are a lethal part of Cordis. They are one of the main ways to level up and earn money. To enter a dungeon, make sure you have an account, and just type `c!startdungeon`. ~~If you want to, you can play a dungeon with up to 3 people.~~ In a dungeon, there will me some different things that will happen.

> Are you a solo player and get annoyed by other people joining your games? Just add `solo` at the end of your command, so no one else can join your party.

## Walking

Most of your time in the dungeon will pass by walking. You don't have to do anything during this part.

## Chests / Loot

Sometimes, you will encounter unlooted chests inside the dungeon. When a chest is found, a button called "Open Chest" will appear. First one to click the button will get the loot inside the chest. But make sure, since some of those chests might be trapped!

## Fighting

During the dungeon you will meet monsters. This will start a fighting sequence. Head to [Fighting](fighting.html) to learn more about fighting.