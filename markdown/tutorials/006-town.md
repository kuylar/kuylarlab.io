# Town
---

In the town, you can buy / sell items, learn new skills, and see your daily quests

## Shop

Shop sells all kinds of items! You can buy and sell potions, armor, weapons and all other kinds of stuff.

## Blacksmith

Blacksmith can upgrade your equipment at your will! Just give him enough money and he will upgrade your stuff within seconds. Please note that this process *might* be a little expensive.

## Skill Shop

From here, you can learn skills! Please note that you will need skill points to learn them. Every time you level up, a skill point will be added to your account.

## Quests

In here, you can find your daily quests. These quests have a 12-hour time limit. If your quests expire, new ones will generate the next time you use any command.