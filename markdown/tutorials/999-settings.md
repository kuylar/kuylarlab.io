# User Settings
---

There are a few settings you edit to change your experience with Cordis. This page explains all of them.

## Hide Discriminator (`hideDiscriminator`)

If someday you end up in the top 10 leaderboards, people can see your discriminator. For your own privacy, you can disable this option. When disabled, your name will be seen as "Username" instead of "Username#1234"