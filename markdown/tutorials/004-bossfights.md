# Boss Fights
---

> TEMPORARILY DISABLED DUE TO SELECT MENU REWRITE

So, I heard you liked playing with this bot, but you don't like the dungeons since they take too much time. Well, good for you, because we made it so you can fight any monster you want! Except they are way more powerful than their dungeon counterpart. Just use `c!fightboss <boss name>` to fight them.

Well, good luck!

> Are you a solo player and get annoyed by other people joining your games? Just add `solo` at the end of your command, so no one else can join your party.