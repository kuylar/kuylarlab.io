# Duelling
---

So, you're into PvP, huh? Well, we have the place for you! Just make sure you're geared enough, and start fighting someone with `c!duel <their mention>`. Please note that you both put 100 coins, and the winner gets them all.

> It's recommended to NOT spend your entire money in a duel, just to get your money back.