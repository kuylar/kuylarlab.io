# Inventory
---

Inventory management can be hard! To make it easier for you to choose items from your inventory, we have developed a "groundbreaking" technology called Stack ID's. You can see them in your inventory, like this:

![Screenshot of a message, showing the inventory of a user, through c!inventory](/img/inventoryembed.png)

> Please note that items can stack up to 99, and by default, your inventory is limited to 9 slots.

Stack ID's are shown before the item name in the inventory, in a darker background. In the screenshot, it is visible that the Stack ID of a Quick-Potion is "`8EB`" (not "`[8EB]`", our testers made that mistake a lot, dont know why)