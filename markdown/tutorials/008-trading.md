# Trading
---

Trading might be confusing, and we understand, but it's actually pretty easy! Just look at this example below:

1. Any of the players start a trade session using `c!trade`
2. The person with the item uses `+<Stack ID> [Amount]` to add the item to their list.
	Examples:
	- `+660` - User adds the item with the stack ID of `660` to their list.
	- `+8EB 3` - User adds 3 items with the stack ID of `660` to their list.
	- `-2` - User takes away the second item from their list
3. The person who pays uses `+C <Amount>` to add money to their list
	Example:
	- `+C 1500` - User adds 1500 coins to their list
	- `-C 500` - User takes away 500 coins from their list
4. Both parties accept using `c!ready`
5. When both parties are ready, the trade completes successfully, and the items and the money goes to their new respective owners

> Please note that steps 2 and 3 can be in different orders, and they can be repeated infinitely

#

> Please remind kuylar#7181 in the [support server](https://discord.gg/pFz4HjD9pR) to add a video example here