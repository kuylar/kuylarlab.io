# Fighting
---

In dungeons, you can fight random monsters. But this might seem hard to some people, since it needs way too much user interaction. This page explains what the buttons (reactions) do. Just make sure that you react when it's your turn. When it's your turn, the message will have a yellow background, as shown in the screenshot below.

![Screenshot of a message, showing a fight state, through c!fightboss](/img/fightembed.png)

## ⚔️ Fight

Clicking "⚔️ Fight" will attack the enemy. You can choose any attack you want from the dropdown that appears right after you click the button. You can learn more skills through `c!skillshop`

## � Item

Clicking "� Item" will let you use an item. These items can:

- Increase your HP
- Increase your SP
- Get rid of a pecific status effect
- and more, in the future