# Accounts
---

Cordis keeps track of your progress through an account system. To create an account, just type in `c!register`. Your account will be crated, and `c!help` will show more entries.

## Account deletion

You can delete your account at any time it's not in use by using `c!unregister`. This will delete ALL your data within Cordis, so *maybe* trade your money and items to someone else before deleting your account.

> Please note that this command is ratelimited for 24h.