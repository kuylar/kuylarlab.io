const fs = require("fs"),
	marked = require("marked"),
	path = require('path'),
	options = {
		gfm: true,
		breaks: true,
		headerIds: true,
		langPrefix: "language-",
		mangle: true,
		silent: true,
		smartLists: true
	},
	template = fs.readFileSync("./template.html").toString();


function getAllFiles(dirPath, arrayOfFiles) {
	let files = fs.readdirSync(dirPath)

	arrayOfFiles = arrayOfFiles || []

	files.forEach(function (file) {
		if (fs.statSync(dirPath + "/" + file).isDirectory()) {
			arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles)
		} else {
			arrayOfFiles.push(path.join(__dirname, dirPath, "/", file))
		}
	})

	return arrayOfFiles
}

console.log("=> Compiling tutorials...");
const tutorialFiles = getAllFiles("../markdown/tutorials");

// Build file list
console.log("==> Building file index");
const tutorialFileIndex = [];
tutorialFiles.forEach(mdfile => {
	const md = fs.readFileSync(mdfile).toString();
	const html = marked(md, options);
	const title = md.split("\n")[0].replace(/#/gi, "").trim();
	const filepath = path.basename(mdfile).split("-").slice(1).join("-").replace(".md", ".html");

	tutorialFileIndex.push({
		title,
		path: filepath,
		hidden: path.basename(mdfile).startsWith("000")
	});
});

console.log("==> Building file list markdown");
let fileIndexMD = "";
tutorialFileIndex.forEach(file => {
	if (file.hidden) return;
	fileIndexMD += `[${file.title}](${file.path})\n`
});
const fileIndexHTML = marked(fileIndexMD, options);

console.log("==> Building articles");
tutorialFiles.forEach(mdfile => {
	const md = fs.readFileSync(mdfile).toString();
	const html = marked(md, options);
	const title = md.split("\n")[0].replace(/#/gi, "").trim();
	const filepath = path.basename(mdfile).split("-").slice(1).join("-").replace(".md", ".html");

	console.log("===> Compiling " + title);

	fs.writeFileSync("../public/cordis/tutorials/" + filepath, template.replace("%%MARKDOWN%%", html).replace("%%TITLE%%", title).replace(/%%FILELIST%%/gi, fileIndexHTML));
});

console.log("Complete!");